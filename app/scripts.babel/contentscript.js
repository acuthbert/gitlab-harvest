'use strict';
/* jshint browser: true */

/**
 * Sets and injects frame.html into the DOM.
 *
 * @return {[type]} [description]
 */
function injectHarvestFrame() {
  chrome.storage.sync.get(['hosts'], function(items) {
    // Parse the Gitlab path and gather parameters to be passed to Harvest
    var pathname = window.location.pathname.split( '/' ),
        group = pathname[1],
        project = pathname[2],
        issue = pathname[4],
        url = window.location.href,
        urlRegex, name, title, search, wrapper, frameURL, iframe;

    if (items && typeof items.hosts !== 'undefined') {
      urlRegex = new RegExp('^(' + (items.hosts.join('|')).replace(/\./g, '\\.') + ')\/[^\/]+\/[^\/]+\/issues\/\\d+', 'i');

      // Make sure the host is approved and it is on issues
      if (window.location.href.match(urlRegex)) {
        // logic for getting github info
        if (window.location.host != 'github.com') {
          search = document.getElementById('search_project_id');
          name = search.dataset.name;
          title = document.getElementsByTagName('h2')[1].innerHTML;
          wrapper = document.getElementsByClassName('content')[0];
        } else {
          title = document.getElementsByClassName('js-issue-title')[0].innerHTML.replace(/^\s+(.*)?\s+$/g, '$1');
          name = document.getElementsByClassName('gh-header-number')[0].innerHTML;
          wrapper = document.getElementById('discussion_bucket');
        }

        // if we have something to append to
        if (wrapper) {
          frameURL = 'frame.html?';
          frameURL += 'issue=' + issue;
          frameURL += '&url=' + encodeURIComponent(url);
          frameURL += '&group=' + encodeURIComponent(group);
          frameURL += '&project=' + encodeURIComponent(project);
          frameURL += '&title=' + encodeURIComponent(title);
          frameURL += '&name=' + encodeURIComponent(name);

          // Embed the harvest iframe at the bottom of the .content div.
          iframe = document.createElement('iframe');
          iframe.src = chrome.runtime.getURL(frameURL);
          iframe.style.cssText = 'width:100%;height:450px;border:0';

          wrapper.appendChild(iframe);
        }
      }
    }
  });
}

// Do it!
injectHarvestFrame();
